
var path = require('path');
var rp = require('request-promise');
var $ = require('cheerio');
var q = require('q');

var log = require(path.resolve(__dirname, 'log.js'));
var db = require(path.resolve(__dirname, 'db.js'));

var nodemailer = require('nodemailer');
var nodemailerTransporter = nodemailer.createTransport({
  service: 'Gmail',
  auth: {
    user: 'foliranje@gmail.com',
    pass: 'foliranti'
  }
});


function replaceSrcAttrs(input, isReverse){
  if(isReverse){
    return input.replace(/_src/gi, 'src');
  }
  return input.replace(/src/gi, '_src');
}

String.prototype.safe = function(){
  return this.trim().replace(/'/g, '');
};

function timestamp(){
  var d = new Date();
  return d.getMinutes() + (d.getHours() * 100) + (d.getDate() * 10000) + ((d.getMonth() + 1) * 1000000) + (d.getFullYear() * 100000000);
}

function doubleDigit(value){
  return value < 10 ? ('0' + value) : value;
}

function datestamp(){
  var date = new Date(),
    y = date.getFullYear(),
    m = date.getMonth() + 1,
    d = date.getDate(),
    hh = date.getHours(),
    mm = date.getMinutes();

  return y + '-' + doubleDigit(m) + '-' + doubleDigit(d) + ' ' + doubleDigit(hh) + ':' + doubleDigit(mm);
}



function PolovniAutomobili(){
  var self = this;

  var urlPrefix = 'http://www.polovniautomobili.com';

  function search(url){
    var cars = [];
    var deferred = q.defer();

    function scrapper(url){
      rp(url).then(
        function(response){
          response = replaceSrcAttrs(response);
          var jqDocument = $(response);
          var jqItems = jqDocument.find('#searchlist-items').find('.item');

          jqItems.each(function(){
            var jqItem = $(this);

            var jqTitle = jqItem.find('.itemtitle a');
            if(!jqTitle.length){
              return true;
            }

            var title = jqTitle.text().safe();
            var link = urlPrefix + jqTitle.attr('href');
            var km = jqItem.find('.title-km').text().safe();
            var year = jqItem.find('.title-year').text().safe();
            var price = jqItem.find('.title-price').text().safe();

            cars.push({
              title: title,
              link: link,
              price: price,
              km: km,
              year: year
            });
          });

          var jqPagination = jqDocument.find('.pagination').eq(0);
          var jqActivePage = jqPagination.find('.selected');
          if(jqActivePage.next().length){
            var jqNextLink = jqActivePage.next().find('a');
            if(jqNextLink.length){
              var nextUrl = jqNextLink.attr('href');
              scrapper(urlPrefix + nextUrl);
              return;
            }
          }
          deferred.resolve(cars);
        },
        function(error){
          log('err', 'URL ERROR:', url, error);
          deferred.resolve([]);
        }
      );
    }

    scrapper(url);

    return deferred.promise;
  }

  self.search = search;
}

function Marktplaats(){
  var self = this;

  function search(url){
    var cars = [];
    var deferred = q.defer();

    function scrapper(url){
      rp(url).then(
        function(response){
          response = replaceSrcAttrs(response);
          var jqDocument = $(response);
          var jqItems = jqDocument.find('article');

          jqItems.each(function(){
            var jqItem = $(this);

            var jqTitle = jqItem.find('.listing-title-description .heading a');
            if(!jqTitle.length){
              //log('info', 'SKIP > not car item!', jqItem);
              return true;
            }

            var title = jqTitle.text().safe();
            var link = jqTitle.attr('href');

            var jqInfo = jqItem.find('.listing-priority-product-container .mp-listing-attributes');
            var km = jqInfo.eq(1).text().safe();
            var year = jqInfo.eq(0).text().safe();

            var price = jqItem.find('.price').text().safe();

            cars.push({
              title: title,
              link: link,
              price: price,
              km: km,
              year: year
            });
          });

          var jqPagination = jqDocument.find('.pagination .pagination-next').eq(0);
          if(jqPagination.length && !jqPagination.hasClass('disabled')){
            var nextUrl = jqPagination.attr('href');
            scrapper(nextUrl);
            return;
          }

          deferred.resolve(cars);
        },
        function(error){
          log('err', 'URL ERROR:', url, error);
          deferred.resolve([]);
        }
      );
    }

    scrapper(url);

    return deferred.promise;
  }

  self.search = search;
}



function go(urls, hander, deferred, allCars){
  if(typeof url === 'string'){
    urls = [urls];
  }
  allCars = allCars || [];

  var url = urls[0];
  urls = urls.splice(1);

  if(!url){
    deferred.resolve(allCars);
    return;
  }

  hander(url).then(
    function(cars){
      allCars.push.apply(allCars, cars);
      go(urls, hander, deferred, allCars);
    }
  );
}

function goall(urls, hander, deferred, allCars){
  if(typeof url === 'string'){
    urls = [urls];
  }
  allCars = allCars || [];

  var qall = [];
  urls.forEach(function(url){
    var handlerPromise = hander(url);
    handlerPromise.then(
      function(cars){
        allCars.push.apply(allCars, cars);
      }
    );
    qall.push(handlerPromise);
  });

  q.all(qall).then(
    function(){
      deferred.resolve(allCars);
    }
  );
}

function validateCarsInDB(cars, website){
  var newCars = [];
  var previousCars = carsCache[website] || [];
  cars.forEach(function(car){
    if(!car.link || previousCars.indexOf(car.link) >= 0){
      return;
    }
    if(!db.isCarInDB(car.link)){
      var isOk = db.insertCarInDB(car);
      if(isOk){
        log('NEW CAR FOUND >', formatCarDetails(car));
        newCars.push(car);
      }else{
        log('err', 'NEW CAR FOUND, BUT INSERT FAILED >', formatCarDetails(car));
      }
    }
  });
  return newCars;
}

function formatCarDetails(car){
  return [car.title, car.km, car.year, car.price].join(' | ');
}

function formatCarDetailsForMail(car){
  return ['<a href="' + car.link + '">' + car.title + '</a>', car.km, car.year, car.price].join(' | ');
}


function sendMail(cars){
  var html = cars
    .map(function(car){
      return formatCarDetailsForMail(car);
    })
    .join('<br><br>');

  var mailOptions = {
    from: 'Cars Test <foliranje@gmail.com>',
    to: 'zwerrr@gmail.com',
    subject: (cars.length < 2 ? 'New car found! ' : 'New cars found! ') + datestamp(),
    html: html
  };

  log('info', 'SEND EMAIL - SENDING...');
  nodemailerTransporter.sendMail(mailOptions, function(error, info){
    if(error){
      log('err', 'SEND EMAIL - FAILED', error);
      return;
    }
    log('info', 'SEND EMAIL - SUCCESS', info.response);
  });
}

var carsCache = {};

global.getCars = function(website, urls){
  var searcher;
  if(website === 'PolovniAutomobli'){
    searcher = new PolovniAutomobili();
  }
  if(website === 'Marktplaats'){
    searcher = new Marktplaats();
  }

  console.time(website);
  var deferred = q.defer();
  deferred.promise.then(
    function(allCars){
      var newCars = validateCarsInDB(allCars, website);
      if(newCars.length){
        sendMail(newCars);
        db.saveDB();
      }
      carsCache[website] = allCars.map(function(car){
        return car.link;
      });
      console.timeEnd(website);
    }
  );

  //go(urls, searcher.search, deferred);
  goall(urls, searcher.search, deferred);
};







global.sloba = function(){

  global.getCars('Marktplaats', ["http://www.marktplaats.nl/z/auto-s/alfa-romeo/147-diesel-handgeschakeld-zilver-of-grijs-zwart-1-4-2-0-liter.html?categoryId=92&searchOnTitleAndDescription=true&priceFrom=450%2C00&priceTo=1.350%2C00&yearFrom=2002&yearTo=2004&mileageFrom=&mileageTo=&attributes=S%2C537&attributes=S%2C474&attributes=S%2C481&attributes=S%2C535&attributes=S%2C466&attributes=S%2C290&attributes=N%2C73&startDateFrom=always","http://www.marktplaats.nl/z/auto-s/alfa-romeo/156-diesel-hatchback-3-5-deurs-sedan-2-4-deurs-handgeschakeld-zilver-of-grijs-zwart-1-4-2-0-liter.html?categoryId=92&searchOnTitleAndDescription=true&priceFrom=450%2C00&priceTo=1.000%2C00&yearFrom=2002&yearTo=2004&mileageFrom=&mileageTo=&attributes=S%2C539&attributes=S%2C474&attributes=S%2C481&attributes=S%2C483&attributes=S%2C535&attributes=S%2C466&attributes=S%2C290&attributes=N%2C73&startDateFrom=always","http://www.marktplaats.nl/z/auto-s/audi/a3-diesel-hatchback-3-5-deurs-sedan-2-4-deurs-handgeschakeld-zilver-of-grijs-zwart-1-4-2-0-liter.html?categoryId=93&searchOnTitleAndDescription=true&attributes=S%2C10882&attributes=S%2C10883&priceFrom=450%2C00&priceTo=2.350%2C00&yearFrom=2002&yearTo=2003&mileageFrom=&mileageTo=&attributes=S%2C560&attributes=S%2C474&attributes=S%2C481&attributes=S%2C483&attributes=S%2C535&attributes=S%2C466&attributes=S%2C290&attributes=N%2C73&startDateFrom=always","http://www.marktplaats.nl/z/auto-s/audi/a3-diesel-hatchback-3-5-deurs-sedan-2-4-deurs-handgeschakeld-zilver-of-grijs-zwart-1-4-2-0-liter.html?categoryId=93&searchOnTitleAndDescription=true&priceFrom=450%2C00&priceTo=3.350%2C00&yearFrom=2004&yearTo=2006&mileageFrom=&mileageTo=&attributes=S%2C560&attributes=S%2C474&attributes=S%2C481&attributes=S%2C483&attributes=S%2C535&attributes=S%2C466&attributes=S%2C290&attributes=N%2C73&startDateFrom=always","http://www.marktplaats.nl/z/auto-s/audi/a4-diesel-sedan-2-4-deurs-handgeschakeld-zilver-of-grijs-zwart-1-4-2-0-liter.html?categoryId=93&searchOnTitleAndDescription=true&attributes=S%2C10882&attributes=S%2C10883&priceFrom=450%2C00&priceTo=3.750%2C00&yearFrom=2002&yearTo=2003&mileageFrom=&mileageTo=&attributes=S%2C561&attributes=S%2C474&attributes=S%2C483&attributes=S%2C535&attributes=S%2C466&attributes=S%2C290&attributes=N%2C73&startDateFrom=always","http://www.marktplaats.nl/z/auto-s/audi/a4-diesel-sedan-2-4-deurs-handgeschakeld-zilver-of-grijs-zwart-1-4-2-0-liter.html?categoryId=93&searchOnTitleAndDescription=true&attributes=S%2C10882&attributes=S%2C10883&priceFrom=450%2C00&priceTo=4.350%2C00&yearFrom=2003&yearTo=2005&mileageFrom=&mileageTo=&attributes=S%2C561&attributes=S%2C474&attributes=S%2C483&attributes=S%2C535&attributes=S%2C466&attributes=S%2C290&attributes=N%2C73&startDateFrom=always","http://www.marktplaats.nl/z/auto-s/audi/a4-diesel-sedan-2-4-deurs-handgeschakeld-zilver-of-grijs-zwart-1-4-2-0-liter.html?categoryId=93&searchOnTitleAndDescription=true&priceFrom=450%2C00&priceTo=5.500%2C00&yearFrom=2005&yearTo=2007&mileageFrom=&mileageTo=&attributes=S%2C561&attributes=S%2C474&attributes=S%2C483&attributes=S%2C535&attributes=S%2C466&attributes=S%2C290&attributes=N%2C73&startDateFrom=always","http://www.marktplaats.nl/z/auto-s/audi/a6-diesel-sedan-2-4-deurs-handgeschakeld-zilver-of-grijs-zwart-1-4-2-0-liter.html?categoryId=93&searchOnTitleAndDescription=true&priceFrom=500%2C00&priceTo=3.500%2C00&yearFrom=2002&yearTo=2004&mileageFrom=&mileageTo=&attributes=S%2C562&attributes=S%2C474&attributes=S%2C483&attributes=S%2C535&attributes=S%2C466&attributes=S%2C290&attributes=N%2C73&startDateFrom=always","http://www.marktplaats.nl/z/auto-s/audi/a6-diesel-sedan-2-4-deurs-handgeschakeld-zilver-of-grijs-zwart-1-4-2-0-liter.html?categoryId=93&searchOnTitleAndDescription=true&priceFrom=500%2C00&priceTo=6.750%2C00&yearFrom=2004&yearTo=2007&mileageFrom=&mileageTo=&attributes=S%2C562&attributes=S%2C474&attributes=S%2C483&attributes=S%2C535&attributes=S%2C466&attributes=S%2C290&attributes=N%2C73&startDateFrom=always","http://www.marktplaats.nl/z/auto-s/bmw/3-serie-diesel-sedan-2-4-deurs-zilver-of-grijs-zwart-1-4-2-0-liter.html?categoryId=96&searchOnTitleAndDescription=true&attributes=S%2C10882&attributes=S%2C10883&priceFrom=500%2C00&priceTo=3.500%2C00&yearFrom=2002&yearTo=2004&mileageFrom=&mileageTo=&attributes=S%2C610&attributes=S%2C474&attributes=S%2C483&attributes=S%2C466&attributes=S%2C290&attributes=N%2C73&startDateFrom=always","http://www.marktplaats.nl/z.html?categoryId=96&attributes=S%2C611&priceFrom=500%2C00&priceTo=2.800%2C00&attributes=S%2C10882&attributes=S%2C10883&yearFrom=2002&yearTo=2003&mileageFrom=&mileageTo=&attributes=S%2C474&attributes=&attributes=&attributes=&query=&searchOnTitleAndDescription=true&postcode=&distance=0&attributes=N%2C73&attributes=S%2C483&attributes=S%2C466&attributes=S%2C290&startDateFrom=ALWAYS&lastPage=l1Cars","http://www.marktplaats.nl/z/auto-s/bmw/5-serie-diesel-sedan-2-4-deurs-zilver-of-grijs-zwart-1-4-2-0-liter.html?categoryId=96&searchOnTitleAndDescription=true&priceFrom=500%2C00&priceTo=7.500%2C00&yearFrom=2005&yearTo=2007&mileageFrom=&mileageTo=&attributes=S%2C611&attributes=S%2C474&attributes=S%2C483&attributes=S%2C466&attributes=S%2C290&attributes=N%2C73&startDateFrom=always","http://www.marktplaats.nl/z.html?categoryId=101&attributes=S%2C673&priceFrom=500%2C00&priceTo=1.450%2C00&attributes=S%2C10882&attributes=S%2C10883&yearFrom=2002&yearTo=2004&mileageFrom=&mileageTo=&attributes=S%2C474&attributes=&attributes=S%2C535&attributes=&query=&searchOnTitleAndDescription=true&postcode=&distance=0&attributes=N%2C73&attributes=S%2C481&attributes=S%2C483&attributes=S%2C287&attributes=S%2C465&attributes=S%2C466&attributes=S%2C290&startDateFrom=ALWAYS&lastPage=l1Cars","http://www.marktplaats.nl/z.html?categoryId=101&attributes=S%2C673&priceFrom=500%2C00&priceTo=1.000%2C00&attributes=S%2C10882&attributes=S%2C10883&yearFrom=2002&yearTo=2004&mileageFrom=&mileageTo=&attributes=S%2C474&attributes=&attributes=S%2C535&attributes=&query=&searchOnTitleAndDescription=true&postcode=&distance=0&attributes=N%2C73&attributes=S%2C484&attributes=S%2C466&attributes=S%2C290&startDateFrom=ALWAYS&lastPage=l1Cars","http://www.marktplaats.nl/z.html?categoryId=111&attributes=S%2C764&priceFrom=450%2C00&priceTo=1.250%2C00&attributes=S%2C10882&attributes=S%2C10883&yearFrom=2002&yearTo=2004&mileageFrom=&mileageTo=&attributes=S%2C474&attributes=&attributes=S%2C535&attributes=&query=&searchOnTitleAndDescription=true&postcode=&distance=0&attributes=N%2C73&attributes=S%2C481&attributes=S%2C483&attributes=S%2C465&attributes=S%2C466&attributes=S%2C290&startDateFrom=ALWAYS&lastPage=l1Cars","http://www.marktplaats.nl/z.html?categoryId=112&attributes=S%2C784&priceFrom=450%2C00&priceTo=1.000%2C00&attributes=S%2C10882&attributes=S%2C10883&yearFrom=2002&yearTo=2004&mileageFrom=&mileageTo=&attributes=S%2C474&attributes=&attributes=S%2C535&attributes=&query=&searchOnTitleAndDescription=true&postcode=&distance=0&attributes=N%2C73&attributes=S%2C481&attributes=S%2C483&attributes=S%2C484&attributes=S%2C466&attributes=S%2C290&startDateFrom=ALWAYS&lastPage=l1Cars","http://www.marktplaats.nl/z.html?categoryId=130&attributes=S%2C966&priceFrom=500%2C00&priceTo=5.250%2C00&attributes=S%2C10882&attributes=S%2C10883&yearFrom=2003&yearTo=2006&mileageFrom=&mileageTo=&attributes=S%2C474&attributes=&attributes=S%2C534&attributes=&query=&searchOnTitleAndDescription=true&postcode=&distance=0&attributes=N%2C74&attributes=S%2C483&attributes=S%2C466&attributes=S%2C290&startDateFrom=ALWAYS&lastPage=l1Cars","http://www.marktplaats.nl/z.html?categoryId=138&attributes=S%2C1064&priceFrom=500%2C00&priceTo=1.600%2C00&attributes=S%2C10882&attributes=S%2C10883&yearFrom=2002&yearTo=2004&mileageFrom=&mileageTo=&attributes=&attributes=&attributes=S%2C535&attributes=&query=&searchOnTitleAndDescription=true&postcode=&distance=0&attributes=N%2C73&attributes=S%2C481&attributes=S%2C483&attributes=S%2C287&attributes=S%2C465&attributes=S%2C466&attributes=S%2C290&startDateFrom=ALWAYS&lastPage=l1Cars","http://www.marktplaats.nl/z.html?categoryId=138&attributes=S%2C1064&priceFrom=500%2C00&priceTo=2.850%2C00&attributes=S%2C10882&attributes=S%2C10883&yearFrom=2004&yearTo=2006&mileageFrom=&mileageTo=&attributes=S%2C474&attributes=&attributes=S%2C535&attributes=&query=&searchOnTitleAndDescription=true&postcode=&distance=0&attributes=N%2C73&attributes=S%2C481&attributes=S%2C483&attributes=S%2C466&attributes=S%2C290&startDateFrom=ALWAYS&lastPage=l1Cars","http://www.marktplaats.nl/z/auto-s/opel/vectra-sedan-2-4-deurs-handgeschakeld-1-4-2-0-liter.html?categoryId=138&searchOnTitleAndDescription=true&attributes=S%2C10882&attributes=S%2C10883&priceFrom=500%2C00&priceTo=1.450%2C00&yearFrom=2002&yearTo=2003&mileageFrom=&mileageTo=&attributes=S%2C1066&attributes=S%2C481&attributes=S%2C483&attributes=S%2C535&attributes=N%2C73&startDateFrom=always","http://www.marktplaats.nl/z.html?categoryId=138&attributes=S%2C1066&priceFrom=500%2C00&priceTo=2.500%2C00&attributes=S%2C10882&attributes=S%2C10883&yearFrom=2003&yearTo=2005&mileageFrom=&mileageTo=&attributes=S%2C474&attributes=&attributes=S%2C535&attributes=&query=&searchOnTitleAndDescription=true&postcode=&distance=0&attributes=N%2C73&attributes=S%2C481&attributes=S%2C483&attributes=S%2C466&attributes=S%2C290&startDateFrom=ALWAYS&lastPage=l1Cars","http://www.marktplaats.nl/z.html?categoryId=138&attributes=S%2C1067&priceFrom=500%2C00&priceTo=1.850%2C00&attributes=S%2C10882&attributes=S%2C10883&yearFrom=2002&yearTo=2004&mileageFrom=&mileageTo=&attributes=S%2C474&attributes=&attributes=S%2C535&attributes=&query=&searchOnTitleAndDescription=true&postcode=&distance=0&attributes=N%2C73&attributes=S%2C466&attributes=S%2C290&startDateFrom=ALWAYS&lastPage=l1Cars","http://www.marktplaats.nl/z.html?categoryId=140&attributes=S%2C1089&priceFrom=500%2C00&priceTo=1.850%2C00&attributes=S%2C10882&attributes=S%2C10883&yearFrom=2002&yearTo=2004&mileageFrom=&mileageTo=&attributes=S%2C474&attributes=&attributes=S%2C535&attributes=&query=&searchOnTitleAndDescription=true&postcode=&distance=0&attributes=N%2C73&attributes=S%2C481&attributes=S%2C483&attributes=S%2C287&attributes=S%2C466&attributes=S%2C290&startDateFrom=ALWAYS&lastPage=l1Cars","http://www.marktplaats.nl/z.html?categoryId=140&attributes=S%2C1089&priceFrom=500%2C00&priceTo=1.850%2C00&attributes=S%2C10882&attributes=S%2C10883&yearFrom=2002&yearTo=2004&mileageFrom=&mileageTo=&attributes=S%2C474&attributes=&attributes=S%2C535&attributes=&query=&searchOnTitleAndDescription=true&postcode=&distance=0&attributes=N%2C73&attributes=S%2C481&attributes=S%2C483&attributes=S%2C287&attributes=S%2C466&attributes=S%2C290&startDateFrom=ALWAYS&lastPage=l1Cars","http://www.marktplaats.nl/z.html?categoryId=140&attributes=S%2C1093&priceFrom=500%2C00&priceTo=1.500%2C00&attributes=S%2C10882&attributes=S%2C10883&yearFrom=2002&yearTo=2004&mileageFrom=&mileageTo=&attributes=S%2C474&attributes=&attributes=S%2C535&attributes=&query=&searchOnTitleAndDescription=true&postcode=&distance=0&attributes=N%2C73&attributes=S%2C481&attributes=S%2C483&attributes=S%2C287&attributes=S%2C465&attributes=S%2C466&attributes=S%2C290&startDateFrom=ALWAYS&lastPage=l1Cars","http://www.marktplaats.nl/z.html?categoryId=140&attributes=S%2C1093&priceFrom=500%2C00&priceTo=1.000%2C00&attributes=S%2C10882&attributes=S%2C10883&yearFrom=2002&yearTo=2004&mileageFrom=&mileageTo=&attributes=S%2C474&attributes=&attributes=S%2C535&attributes=&query=&searchOnTitleAndDescription=true&postcode=&distance=0&attributes=N%2C73&attributes=S%2C484&attributes=S%2C287&attributes=S%2C465&attributes=S%2C466&attributes=S%2C290&startDateFrom=ALWAYS&lastPage=l1Cars","http://www.marktplaats.nl/z/auto-s/peugeot/407-diesel-hatchback-3-5-deurs-sedan-2-4-deurs-handgeschakeld-blauw-rood-zilver-of-grijs-zwart-1-4-2-0-liter.html?categoryId=140&searchOnTitleAndDescription=true&attributes=S%2C10882&attributes=S%2C10883&priceFrom=500%2C00&priceTo=3.100%2C00&yearFrom=2004&yearTo=2006&mileageFrom=&mileageTo=&attributes=S%2C1100&attributes=S%2C474&attributes=S%2C481&attributes=S%2C483&attributes=S%2C535&attributes=S%2C287&attributes=S%2C465&attributes=S%2C466&attributes=S%2C290&attributes=N%2C73&startDateFrom=always","http://www.marktplaats.nl/z.html?categoryId=140&attributes=S%2C1098&priceFrom=500%2C00&priceTo=3.000%2C00&attributes=S%2C10882&attributes=S%2C10883&yearFrom=&yearTo=&mileageFrom=&mileageTo=&attributes=S%2C474&attributes=&attributes=S%2C535&attributes=&query=&searchOnTitleAndDescription=true&postcode=&distance=0&attributes=N%2C73&attributes=S%2C481&attributes=S%2C483&attributes=S%2C287&attributes=S%2C465&attributes=S%2C466&attributes=S%2C290&startDateFrom=ALWAYS&lastPage=l1Cars","http://www.marktplaats.nl/z.html?categoryId=140&attributes=S%2C1102&priceFrom=&priceTo=3.000%2C00&attributes=S%2C10882&attributes=S%2C10883&yearFrom=2005&yearTo=2006&mileageFrom=&mileageTo=&attributes=S%2C474&attributes=&attributes=S%2C535&attributes=&query=&searchOnTitleAndDescription=true&postcode=&distance=0&attributes=N%2C73&attributes=S%2C483&attributes=S%2C466&attributes=S%2C290&startDateFrom=ALWAYS&lastPage=l1Cars","http://www.marktplaats.nl/z.html?categoryId=157&attributes=S%2C1260&priceFrom=500%2C00&priceTo=2.250%2C00&attributes=S%2C10882&attributes=S%2C10883&yearFrom=2002&yearTo=2004&mileageFrom=&mileageTo=&attributes=S%2C474&attributes=&attributes=S%2C535&attributes=&query=&searchOnTitleAndDescription=true&postcode=&distance=0&attributes=N%2C73&attributes=S%2C481&attributes=S%2C483&attributes=S%2C466&attributes=S%2C290&startDateFrom=ALWAYS&lastPage=l1Cars","http://www.marktplaats.nl/z.html?categoryId=157&attributes=S%2C1260&priceFrom=500%2C00&priceTo=4.000%2C00&attributes=S%2C10882&attributes=S%2C10883&yearFrom=2004&yearTo=2006&mileageFrom=&mileageTo=&attributes=S%2C474&attributes=&attributes=S%2C535&attributes=&query=&searchOnTitleAndDescription=true&postcode=&distance=0&attributes=N%2C73&attributes=S%2C481&attributes=S%2C483&attributes=S%2C466&attributes=S%2C290&startDateFrom=ALWAYS&lastPage=l1Cars","http://www.marktplaats.nl/z.html?categoryId=157&attributes=S%2C1261&priceFrom=500%2C00&priceTo=3.000%2C00&attributes=S%2C10882&attributes=S%2C10883&yearFrom=2002&yearTo=2003&mileageFrom=&mileageTo=&attributes=S%2C474&attributes=&attributes=S%2C535&attributes=&query=&searchOnTitleAndDescription=true&postcode=&distance=0&attributes=N%2C73&attributes=S%2C483&attributes=S%2C466&attributes=S%2C290&startDateFrom=ALWAYS&lastPage=l1Cars","http://www.marktplaats.nl/z.html?categoryId=157&attributes=S%2C1261&priceFrom=500%2C00&priceTo=3.500%2C00&attributes=S%2C10882&attributes=S%2C10883&yearFrom=2004&yearTo=2005&mileageFrom=&mileageTo=&attributes=S%2C474&attributes=&attributes=S%2C535&attributes=&query=&searchOnTitleAndDescription=true&postcode=&distance=0&attributes=N%2C73&attributes=S%2C483&attributes=S%2C466&attributes=S%2C290&startDateFrom=ALWAYS&lastPage=l1Cars","http://www.marktplaats.nl/z.html?categoryId=157&attributes=S%2C1261&priceFrom=500%2C00&priceTo=5.150%2C00&attributes=S%2C10882&attributes=S%2C10883&yearFrom=2005&yearTo=2007&mileageFrom=&mileageTo=&attributes=S%2C474&attributes=&attributes=S%2C535&attributes=&query=&searchOnTitleAndDescription=true&postcode=&distance=0&attributes=N%2C73&attributes=S%2C483&attributes=S%2C466&attributes=S%2C290&startDateFrom=ALWAYS&lastPage=l1Cars"]);

};

global.pa = function(){

  global.getCars('PolovniAutomobli', ['http://www.polovniautomobili.com/putnicka-vozila/pretraga?submit_1=&brand=192&model=1824&price_from=&price_to=6000&year_from=2010&year_to=&fuel%5B%5D=2309&door_num=&without_price=1&date_limit=&showOldNew=all&modeltxt=&engine_volume_from=&engine_volume_to=&power_from=&power_to=&mileage_from=&mileage_to=&emission_class=&seat_num=&wheel_side=&registration=&country=&city=&page=&sort=', 'http://www.polovniautomobili.com/putnicka-vozila/pretraga?submit_1=&brand=192&model=1824&price_from=&price_to=8000&year_from=2012&year_to=&fuel%5B%5D=2309&door_num=&without_price=1&date_limit=&showOldNew=all&modeltxt=&engine_volume_from=&engine_volume_to=&power_from=&power_to=&mileage_from=&mileage_to=&emission_class=&seat_num=&wheel_side=&registration=&country=&city=&page=&sort=', 'http://www.polovniautomobili.com/putnicka-vozila/pretraga?submit_1=&brand=56&model=1308&price_from=&price_to=8000&year_from=2012&year_to=&fuel%5B%5D=2309&door_num=&without_price=1&date_limit=&showOldNew=all&modeltxt=&engine_volume_from=&engine_volume_to=&power_from=&power_to=&mileage_from=&mileage_to=&emission_class=&seat_num=&wheel_side=&registration=&country=&city=&page=&sort=', 'http://www.polovniautomobili.com/putnicka-vozila/pretraga?submit_1=&brand=181&model=1695&price_from=&price_to=&year_from=2008&year_to=&fuel%5B%5D=2309&door_num=&without_price=1&date_limit=&showOldNew=all&modeltxt=&engine_volume_from=&engine_volume_to=&power_from=&power_to=&mileage_from=&mileage_to=&emission_class=&seat_num=&wheel_side=&registration=&country=&city=&page=&sort=']);

};

global.pa24 = function(){

  global.getCars('PolovniAutomobli', ['http://www.polovniautomobili.com/putnicka-vozila/poslednja24h']);

};

setInterval(global.sloba, 30 * 1000);
global.sloba();
