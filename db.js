
var path = require('path');
var fs = require('fs');
var sql = require('sql.js');
var log = require(path.resolve(__dirname, 'log.js'));

function timestamp(){
  var d = new Date();
  return d.getMinutes() + (d.getHours() * 100) + (d.getDate() * 10000) + ((d.getMonth() + 1) * 1000000) + (d.getFullYear() * 100000000);
}

function DB(){
  var self = this;

  var bfr = fs.readFileSync('cars.sqlite');
  var db = new sql.Database(bfr);

  function insertCarInDB(carObject){
    var isSuccess = true;

    try{
      var props = [];
      var values = [];
      for(var prop in carObject){
        props.push(prop);
        values.push(carObject[prop]);
      }
      var propsString = '(' + props.join(',') + ',timestamp)';
      var valuesString = "('" + values.join("','") + "'," + timestamp() + ")";
      var sql = 'INSERT INTO cars ' + propsString + ' VALUES ' + valuesString;

      db.run(sql);
    }catch(exception){
      isSuccess = false;
      log('err', 'Insert in DB', carObject.link, exception);
    }

    return isSuccess;
  }

  function isCarInDB(link){
    try{
      var result = query('SELECT link FROM cars WHERE link = "' + link + '"');
      return result[0].values.length > 0;
    }catch(exception){
      return false;
    }
  }

  function query(sql){
    return db.exec(sql);
  }

  function closeDatabase(){
    db.close();
  }

  function saveDB(){
    console.time('SAVE DB TO DISK');
    var data = db.export();
    var buffer = new Buffer(data);
    fs.writeFileSync('cars.sqlite', buffer);
    console.timeEnd('SAVE DB TO DISK');
  }

  self.insertCarInDB = insertCarInDB;
  self.isCarInDB = isCarInDB;
  self.query = query;
  self.saveDB = saveDB;
}

module.exports = new DB();
