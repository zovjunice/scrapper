
var types = {
  ok: 'gray',
  err: 'red',
  warn: 'orange',
  info: 'blue'
};


var log = function(){
  var args = Array.prototype.slice.call(arguments);
  var type = args[0];
  if(types.hasOwnProperty(type)){
    args = args.slice(1);
  }else{
    type = 'ok';
  }

  //args.unshift('color:' + types[type] + ';font-weight:bold');
  //args.unshift('%c%s');

  console.log.apply(console, args);
};

module.exports = log;
